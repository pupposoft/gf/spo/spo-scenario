package br.com.pupposoft.spo.scenario.gateway.http.controllers.json;

import java.util.List;
import java.util.stream.Collectors;

import br.com.pupposoft.spo.scenario.domains.Scenario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ScenarioJson {
	private String id;
	private String name;
	private String description;
	private String userIdOwner;
	private List<UserJson> users;
	
	public ScenarioJson(final Scenario scenario) {
		this.id = scenario.getId();
		this.name = scenario.getName();
		this.description = scenario.getDescription();
		this.userIdOwner = scenario.getUserIdOwner();
		
		this.users = scenario.getUsers()
				.stream()
				.map(UserJson::new)
				.collect(Collectors.toList());
	}
}
