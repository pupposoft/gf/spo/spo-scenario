package br.com.pupposoft.spo.scenario.gateway.database;

import java.util.List;

import br.com.pupposoft.spo.scenario.domains.Scenario;

public interface ScenarioDBGateway {
	String save(Scenario scenario);
	List<Scenario> findByUserId(String id);
	Scenario findById(String scenarioId);
	void deleteById(String scenarioId);
}
