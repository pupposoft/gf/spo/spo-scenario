package br.com.pupposoft.spo.scenario.gateway.database.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class ErrorToAccessDatabaseGatewayException extends RuntimeException {
	private static final long serialVersionUID = -976036540532770474L;

	//TODO: criar repositorio da starter exception
	
	private String code = "spo.scenario.errorToAccessScenarioDatabase";
	private String message = "Error to access scenario database.";
	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

}
