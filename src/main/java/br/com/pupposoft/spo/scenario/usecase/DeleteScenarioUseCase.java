package br.com.pupposoft.spo.scenario.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.pupposoft.spo.scenario.domains.Scenario;
import br.com.pupposoft.spo.scenario.gateway.database.ScenarioDBGateway;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DeleteScenarioUseCase extends ScenarioBaseUseCase {
	
	@Autowired
	private GetScenarioUseCase getScenarioUseCase;
	
	@Autowired
	private ScenarioDBGateway scenarioDBGateway; 

	public void delete(String scenarioId, String userEmail) {
		log.info("Start scenarioId={}, userEmail={}", scenarioId, userEmail);
		Scenario scenarioToDelete = getScenarioUseCase.getByUserOwner(scenarioId, userEmail);
		scenarioDBGateway.deleteById(scenarioToDelete.getId());
		log.info("End scenarioId={}, userEmail={}", scenarioId, userEmail);
	}
}
