package br.com.pupposoft.spo.scenario.gateway.user.rest;

import java.util.Optional;

import org.springframework.stereotype.Component;

import br.com.pupposoft.spo.scenario.domains.User;
import br.com.pupposoft.spo.scenario.gateway.user.UserGateway;

@Component
public class ScenarioDirectAccessToUser implements UserGateway {

	@Override
	public Optional<User> findByEmail(String userEmail) {
		// TODO implementar esse método com retorno real.
		final User user = new User("anyId"); 
		Optional<User> userOp = Optional.of(user); 
		return userOp;
	}

}
