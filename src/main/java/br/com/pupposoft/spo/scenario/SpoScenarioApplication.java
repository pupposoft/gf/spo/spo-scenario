package br.com.pupposoft.spo.scenario;

import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpoScenarioApplication {
    public static void main(String[] args) throws Exception {
    	Locale.setDefault(new Locale( "pt", "BR" ));
        SpringApplication.run(SpoScenarioApplication.class, args);
    }
}
