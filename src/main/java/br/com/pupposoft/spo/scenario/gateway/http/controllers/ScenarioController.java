package br.com.pupposoft.spo.scenario.gateway.http.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pupposoft.spo.scenario.domains.Scenario;
import br.com.pupposoft.spo.scenario.gateway.http.controllers.json.ScenarioJson;
import br.com.pupposoft.spo.scenario.usecase.DeleteScenarioUseCase;
import br.com.pupposoft.spo.scenario.usecase.GetScenarioUseCase;
import br.com.pupposoft.spo.scenario.usecase.SaveScenarioUseCase;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("scenarios")
public class ScenarioController {
	
	@Autowired
	private SaveScenarioUseCase saveScenarioUseCase;
	
	@Autowired
	private GetScenarioUseCase getScenarioUseCase;
	
	@Autowired
	private DeleteScenarioUseCase deleteScenarioUseCase;
	
	@PostMapping
	public String create(
			@RequestBody(required = true) final ScenarioJson scenarioJson, 
			@RequestHeader(name = "logged-in-user-email", required = true) final String userEmail) {
		final Scenario scenario = mapperJsonToScenario(scenarioJson);
		return saveScenarioUseCase.create(scenario, userEmail);
	}
	
	@GetMapping("{id}")
	public Scenario get(
			@RequestBody(required = true) final String scenarioId,
			@RequestHeader(name = "logged-in-user-email", required = true) final String userEmail) {
		return getScenarioUseCase.get(scenarioId, userEmail);
	}
		
	@GetMapping
	public List<ScenarioJson> getAll(
			@RequestHeader(name = "logged-in-user-email", required = true) final String userEmail) {
		List<Scenario> scenarios = getScenarioUseCase.getAll(userEmail);
		return mapperScenarioToJson(scenarios);
	}
	
	@PatchMapping
	public void update(
			@RequestBody(required = true) final ScenarioJson scenarioJson, 
			@RequestHeader(name = "logged-in-user-email", required = true) final String userEmail) {
		final Scenario scenario = mapperJsonToScenario(scenarioJson);
		saveScenarioUseCase.update(scenario, userEmail);
	}
	
	@DeleteMapping("{id}")
	public void delete(
			@PathVariable(name = "id", required = true) final String scenarioId, 
			@RequestHeader(name = "logged-in-user-email", required = true) final String userEmail) {
		log.info("Start scenarioId={}, userEmail={}", scenarioId, userEmail);
		deleteScenarioUseCase.delete(scenarioId, userEmail);
		log.info("End scenarioId={}, userEmail={}", scenarioId, userEmail);
	}

	private Scenario mapperJsonToScenario(final ScenarioJson scenarioJson) {
		final Scenario scenario = new Scenario(
				scenarioJson.getId(), 
				scenarioJson.getName(), 
				scenarioJson.getDescription(), 
				null, null);
		return scenario;
	}
	
	private List<ScenarioJson> mapperScenarioToJson(List<Scenario> scenarios) {
		return scenarios.stream()
					.map(ScenarioJson::new)
					.collect(Collectors.toList());
	}

}
