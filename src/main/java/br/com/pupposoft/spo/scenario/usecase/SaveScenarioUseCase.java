package br.com.pupposoft.spo.scenario.usecase;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.pupposoft.spo.scenario.domains.Scenario;
import br.com.pupposoft.spo.scenario.domains.User;
import br.com.pupposoft.spo.scenario.gateway.database.ScenarioDBGateway;

@Service
public class SaveScenarioUseCase extends ScenarioBaseUseCase {
	
	@Autowired
	private ScenarioDBGateway scenarioDBGateway;
	
	@Autowired
	private GetScenarioUseCase getScenarioUseCase;
	
	public String create(Scenario scenario, String userEmail) {
		User userOwner = getUserByEmail(userEmail);
		Scenario scenarioToSave = generateScenarioToSave(scenario, userOwner);
		return scenarioDBGateway.save(scenarioToSave);
	}

	private Scenario generateScenarioToSave(Scenario scenario, User userOwner) {
		Scenario scenarioToSave = new Scenario(
				null, 
				scenario.getName(), 
				scenario.getDescription(), 
				userOwner.getId(), 
				new ArrayList<>());
		return scenarioToSave;
	}

	public void update(Scenario scenario, String userEmail) {
		final Scenario scenarioExistent = getScenarioUseCase.get(scenario.getId(), userEmail);
		final Scenario scenarioToUpdate = generateScenarioToUpdate(scenario, scenarioExistent);
		scenarioDBGateway.save(scenarioToUpdate);
	}

	private Scenario generateScenarioToUpdate(Scenario scenario, final Scenario scenarioExistent) {
		final Scenario scenarioToUpdate = new Scenario(
				scenarioExistent.getId(), 
				scenario.getName(), 
				scenario.getDescription(), 
				scenarioExistent.getUserIdOwner(), 
				scenarioExistent.getUsers());
		return scenarioToUpdate;
	}

}
