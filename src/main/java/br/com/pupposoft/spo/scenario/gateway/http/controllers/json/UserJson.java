package br.com.pupposoft.spo.scenario.gateway.http.controllers.json;

import br.com.pupposoft.spo.scenario.domains.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserJson {
	private String id;

	public UserJson(final User user) {
		this.id = user.getId();
	}
}
