package br.com.pupposoft.spo.scenario.gateway.user;

import java.util.Optional;

import br.com.pupposoft.spo.scenario.domains.User;

public interface UserGateway {
	Optional<User> findByEmail(String userEmail); 
}
