package br.com.pupposoft.spo.scenario.usecase;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.pupposoft.spo.scenario.domains.User;
import br.com.pupposoft.spo.scenario.gateway.user.UserGateway;

public abstract class ScenarioBaseUseCase {
	
	@Autowired
	private UserGateway userGateway;
	
	public User getUserByEmail(String userEmail) {
		Optional<User> userOp = userGateway.findByEmail(userEmail);
		return userOp.get();
	}

}
