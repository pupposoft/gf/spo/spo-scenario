package br.com.pupposoft.spo.scenario.usecase;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.pupposoft.spo.scenario.domains.Scenario;
import br.com.pupposoft.spo.scenario.domains.User;
import br.com.pupposoft.spo.scenario.gateway.database.ScenarioDBGateway;
import br.com.pupposoft.spo.scenario.usecase.exception.UserUnauthorizedBusinessException;

@Service
public class GetScenarioUseCase extends ScenarioBaseUseCase {
	
	@Autowired
	private ScenarioDBGateway scenarioDBGateway;

	public Scenario get(String scenarioId, String userEmail) {
		User user = getUserByEmail(userEmail);
		Scenario scenario = scenarioDBGateway.findById(scenarioId);
		boolean userHasPermission = scenario.hasPermission(user.getId());
		if(userHasPermission) {
			return scenario;
		} 
		throw new UserUnauthorizedBusinessException();
	}
	
	public Scenario getByUserOwner(String scenarioId, String userEmail) {
		User user = getUserByEmail(userEmail);
		Scenario scenario = scenarioDBGateway.findById(scenarioId);
		boolean isUserOwner = scenario.isOwner(user.getId());
		if(isUserOwner) {
			return scenario;
		}
		throw new UserUnauthorizedBusinessException();
	}
	
	public List<Scenario> getAll(String userEmail) {
		User user = getUserByEmail(userEmail);
		return scenarioDBGateway.findByUserId(user.getId());
	}

}
