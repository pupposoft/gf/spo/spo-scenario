package br.com.pupposoft.spo.scenario.domains;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Scenario {
	private String id;
	private String name;
	private String description;
	private String userIdOwner;
	private List<User> users;
	
	public boolean isOwner(String userId) {
		return userId.equals(this.userIdOwner);
	}

	public boolean hasPermission(String userId) {
		if(isOwner(userId)) {
			return true;
		}

		return users
				.stream()
				.anyMatch(user -> user.getId().equals(userId));
	}
}
