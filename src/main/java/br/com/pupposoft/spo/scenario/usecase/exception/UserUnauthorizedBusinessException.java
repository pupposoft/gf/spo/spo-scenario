package br.com.pupposoft.spo.scenario.usecase.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class UserUnauthorizedBusinessException extends RuntimeException {
	private static final long serialVersionUID = 6975176937798811796L;
	
	//TODO: criar repositorio da starter exception
	
	private String code = "spo.scenario.userUnauthorized";
	private String message = "User unauthorized to execute this operation.";
	private HttpStatus httpStatus = HttpStatus.NOT_FOUND;

}
