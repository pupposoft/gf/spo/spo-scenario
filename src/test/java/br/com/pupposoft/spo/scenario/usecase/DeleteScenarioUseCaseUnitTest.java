package br.com.pupposoft.spo.scenario.usecase;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.pupposoft.spo.scenario.domains.Scenario;
import br.com.pupposoft.spo.scenario.domains.User;
import br.com.pupposoft.spo.scenario.gateway.database.ScenarioDBGateway;

@ExtendWith(MockitoExtension.class)
public class DeleteScenarioUseCaseUnitTest {
	
	@InjectMocks
	private DeleteScenarioUseCase deleteScenarioUseCase;
	
	@Mock
	private GetScenarioUseCase getScenarioUseCase;
	
	@Mock
	private ScenarioDBGateway scenarioDBGateway;
	
	@Test
	public void deleteWithSuccess() {
		final String scenarioId = "anyScenarioId";
		final String name = "anyName";
		final String description = "anyDescription";
		final String userIdOwner = "anyIdOwner";
		final String userEmail = "anyEmail";
		final List<User> users = new ArrayList<>();
		
		final Scenario scenarioToDelete = new Scenario(scenarioId, name, description, userIdOwner, users);
		doReturn(scenarioToDelete).when(getScenarioUseCase).getByUserOwner(scenarioId, userEmail);
		
		deleteScenarioUseCase.delete(scenarioId, userEmail);
		
		verify(getScenarioUseCase).getByUserOwner(scenarioId, userEmail);
		verify(scenarioDBGateway).deleteById(scenarioToDelete.getId());
	}
}
