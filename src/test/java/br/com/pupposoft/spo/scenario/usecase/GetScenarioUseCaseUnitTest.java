package br.com.pupposoft.spo.scenario.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.pupposoft.spo.scenario.domains.Scenario;
import br.com.pupposoft.spo.scenario.domains.User;
import br.com.pupposoft.spo.scenario.gateway.database.ScenarioDBGateway;
import br.com.pupposoft.spo.scenario.gateway.user.UserGateway;
import br.com.pupposoft.spo.scenario.usecase.exception.UserUnauthorizedBusinessException;

@ExtendWith(MockitoExtension.class)
public class GetScenarioUseCaseUnitTest {
	
	@InjectMocks
	private GetScenarioUseCase getScenarioUseCase;
	
	@Mock
	private ScenarioDBGateway scenarioDBGateway;
	
	@Mock
	private UserGateway userGateway;
	
	@Test
	public void getAll() {
		final String userEmail = "anyEmail";
		final String userId = "anyIdOwner";
		final User user = new User(userId);
		
		final List<User> users = new ArrayList<>();
		users.add(new User("AA"));
		users.add(new User("BB"));
		users.add(new User("CC"));
		
		final List<Scenario> scenarios = new ArrayList<>();
		scenarios.add(new Scenario("sc1", "casa", "anyDescription", "anyIdOwner", users));
		scenarios.add(new Scenario("sc2", "apt", "anyDescription", "anyIdOwner", users));
		
		Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		doReturn(scenarios).when(scenarioDBGateway).findByUserId(userId);
		
		List<Scenario> scenariosReturned = getScenarioUseCase.getAll(userEmail);
		
		verify(userGateway).findByEmail(userEmail);
		verify(scenarioDBGateway).findByUserId(userId);
		
		assertEquals(2, scenariosReturned.size());
		assertEquals(3, scenariosReturned.get(0).getUsers().size());
		assertEquals(3, scenariosReturned.get(1).getUsers().size());
		
		assertEquals("AA", scenariosReturned.get(0).getUsers().get(0).getId());
		assertEquals("BB", scenariosReturned.get(0).getUsers().get(1).getId());
		assertEquals("CC", scenariosReturned.get(0).getUsers().get(2).getId());
		
		assertEquals("sc1", scenariosReturned.get(0).getId());
		assertEquals("casa", scenariosReturned.get(0).getName());
		assertEquals("anyIdOwner", scenariosReturned.get(0).getUserIdOwner());
		
		assertEquals("sc2", scenariosReturned.get(1).getId());
		assertEquals("apt", scenariosReturned.get(1).getName());
		assertEquals("anyIdOwner", scenariosReturned.get(1).getUserIdOwner());
	}
	
	@Test
	public void get() {
		final String scenarioId = "anyScenarioId";
		final String name = "anyName";
		final String description = "anyDescription";
		final String userIdOwner = "anyIdOwner";
		final String userEmail = "anyEmail";
		final String userId = "BB";
		final User user = new User(userId);
		
		final List<User> users = new ArrayList<>();
		users.add(new User("AA"));
		users.add(new User("BB"));
		users.add(new User("CC"));
		
		Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		final Scenario scenario = new Scenario(scenarioId, name, description, userIdOwner, users);
		doReturn(scenario).when(scenarioDBGateway).findById(scenarioId);
		
		Scenario scenarioFound = getScenarioUseCase.get(scenarioId, userEmail);
		
		verify(userGateway).findByEmail(userEmail);
		verify(scenarioDBGateway).findById(scenarioId);
		
		assertEquals(scenarioId, scenarioFound.getId());
		assertEquals(name, scenarioFound.getName());
		assertEquals(description, scenarioFound.getDescription());
		assertEquals(userIdOwner, scenarioFound.getUserIdOwner());
		assertEquals(3, scenarioFound.getUsers().size());
		assertEquals("AA", scenarioFound.getUsers().get(0).getId());
		assertEquals("BB", scenarioFound.getUsers().get(1).getId());
		assertEquals("CC", scenarioFound.getUsers().get(2).getId());
	}
	
	@Test
	public void getUserUnauthorizedBusinessException() {
		final String scenarioId = "anyScenarioId";
		final String name = "anyName";
		final String description = "anyDescription";
		final String userIdOwner = "anyIdOwner";
		final String userEmail = "anyEmail";
		final String userId = "anyUserId";
		final User user = new User(userId);
		
		final List<User> users = new ArrayList<>();
		users.add(new User("AA"));
		users.add(new User("BB"));
		users.add(new User("CC"));
		
		Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		final Scenario scenario = new Scenario(scenarioId, name, description, userIdOwner, users);
		doReturn(scenario).when(scenarioDBGateway).findById(scenarioId);
		
		assertThrows(UserUnauthorizedBusinessException.class, () -> {
			getScenarioUseCase.get(scenarioId, userEmail);
		});
		
		verify(userGateway).findByEmail(userEmail);
		verify(scenarioDBGateway).findById(scenarioId);
	}
	
	@Test
	public void getByUserOwner() {
		final String scenarioId = "anyScenarioId";
		final String name = "anyName";
		final String description = "anyDescription";
		final String userIdOwner = "anyIdOwner";
		final String userEmail = "anyEmail";
		final User userFound = new User(userIdOwner);
		
		final List<User> users = new ArrayList<>();
		users.add(new User("Aa"));
		users.add(new User("Bb"));
		
		Optional<User> userOp = Optional.of(userFound);
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		final Scenario scenario = new Scenario(scenarioId, name, description, userIdOwner, users);
		doReturn(scenario).when(scenarioDBGateway).findById(scenarioId);
		
		Scenario scenarioExistent = getScenarioUseCase.getByUserOwner(scenarioId, userEmail);
		
		verify(userGateway).findByEmail(userEmail);
		verify(scenarioDBGateway).findById(scenarioId);
		
		assertEquals(scenarioId, scenarioExistent.getId());
		assertEquals(name, scenarioExistent.getName());
		assertEquals(description, scenarioExistent.getDescription());
		assertEquals(userIdOwner, scenarioExistent.getUserIdOwner());
		assertEquals(users.get(0).getId(), scenarioExistent.getUsers().get(0).getId());
		assertEquals(users.get(1).getId(), scenarioExistent.getUsers().get(1).getId());
	}
	
	@Test
	public void getByUserUnauthorizedBusinessException() {
		final String scenarioId = "anyScenarioId";
		final String name = "anyName";
		final String description = "anyDescription";
		final String userIdOwner = "anyIdOwner";
		final String userEmail = "anyEmail";
		final User userFound = new User("Aa");
		
		final List<User> users = new ArrayList<>();
		users.add(userFound);
		
		Optional<User> userOp = Optional.of(userFound);
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		final Scenario scenario = new Scenario(scenarioId, name, description, userIdOwner, users);
		doReturn(scenario).when(scenarioDBGateway).findById(scenarioId);
		
		assertThrows(UserUnauthorizedBusinessException.class, () -> {
			getScenarioUseCase.getByUserOwner(scenarioId, userEmail);
		});
		
		verify(userGateway).findByEmail(userEmail);
		verify(scenarioDBGateway).findById(scenarioId);
	}

}
