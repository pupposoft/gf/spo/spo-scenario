package br.com.pupposoft.spo.scenario.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.pupposoft.spo.scenario.domains.Scenario;
import br.com.pupposoft.spo.scenario.domains.User;
import br.com.pupposoft.spo.scenario.gateway.database.ScenarioDBGateway;
import br.com.pupposoft.spo.scenario.gateway.user.UserGateway;

@ExtendWith(MockitoExtension.class)
public class SaveScenarioUseCaseUnitTest {
	
	@InjectMocks
	private SaveScenarioUseCase saveScenarioUseCase;
	
	@Mock
	private UserGateway userGateway;
	
	@Mock
	private ScenarioDBGateway scenarioDBGateway;
	
	@Mock
	private GetScenarioUseCase getScenarioUseCase;
	
	@Test
	public void createWithSuccess() {
		final String userEmail = "anyEmail";
		final String userId = "anyId";
		final String name = "anyName";
		final String description = "anyDescription";
		final String idToReturn = "anyId";
		final User user = new User(userId);
		
		final Scenario scenario = new Scenario(null, name, description, null, null);
		
		Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		doReturn(idToReturn).when(scenarioDBGateway).save(any(Scenario.class));
		
		String idReturned = saveScenarioUseCase.create(scenario, userEmail);

		verify(userGateway).findByEmail(userEmail);
		
		ArgumentCaptor<Scenario> scenarioAC = ArgumentCaptor.forClass(Scenario.class);
		verify(scenarioDBGateway).save(scenarioAC.capture());
		
		Scenario scenarioSaved = scenarioAC.getValue();
		
		assertEquals(idToReturn, idReturned);
		assertEquals(name, scenarioSaved.getName());
		assertEquals(description, scenarioSaved.getDescription());
		assertEquals(userId, scenarioSaved.getUserIdOwner());
		assertNull(scenarioSaved.getId());
		assertNotNull(scenarioSaved.getUsers());
		assertTrue(scenarioSaved.getUsers().isEmpty());
	}
	
	@Test
	public void update() {
		final String userEmail = "anyEmail";
		final String scenarioId = "anyScenarioId"; 
		final String name = "anyName";
		final String description = "anyDescription";
		final String userIdOwner = "anyIdOwner";
		final List<User> users = new ArrayList<>();
		users.add(new User("A1"));
		
		final Scenario scenarioToUpdate = new Scenario(scenarioId,"conta", "poupança", null, null);
		
		final Scenario scenarioExistent = new Scenario(scenarioId, name, description, userIdOwner, users);
		doReturn(scenarioExistent).when(getScenarioUseCase).get(scenarioToUpdate.getId(), userEmail);
		
		saveScenarioUseCase.update(scenarioToUpdate, userEmail);

		verify(getScenarioUseCase).get(scenarioId, userEmail);
		
		ArgumentCaptor<Scenario> scenarioAC = ArgumentCaptor.forClass(Scenario.class);
		verify(scenarioDBGateway).save(scenarioAC.capture());
		
		Scenario scenarioUpdated = scenarioAC.getValue();
		
		assertEquals(scenarioToUpdate.getId(), scenarioUpdated.getId());
		assertEquals(scenarioToUpdate.getName(), scenarioUpdated.getName());
		assertEquals(scenarioToUpdate.getDescription(), scenarioUpdated.getDescription());
		assertEquals(users.get(0).getId(), scenarioUpdated.getUsers().get(0).getId());
		assertEquals(1, scenarioUpdated.getUsers().size());
		assertEquals(userIdOwner, scenarioUpdated.getUserIdOwner());
	}

}
