package br.com.pupposoft.spo.scenario.gateway.database.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

public class ErrorToAccessDatabaseGatewayExceptionUnitTest {
	
	@Test
	public void errorToAccessDatabaseGateway() {
		ErrorToAccessDatabaseGatewayException exception = new ErrorToAccessDatabaseGatewayException();
		assertEquals("spo.scenario.errorToAccessScenarioDatabase", exception.getCode());
		assertEquals("Error to access scenario database.", exception.getMessage());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getHttpStatus());
	}
}
