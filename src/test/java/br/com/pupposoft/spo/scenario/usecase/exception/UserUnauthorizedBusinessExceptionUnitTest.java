package br.com.pupposoft.spo.scenario.usecase.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

public class UserUnauthorizedBusinessExceptionUnitTest {
	
	@Test
	public void userUnauthorizedBusinessException() {
		UserUnauthorizedBusinessException e = new UserUnauthorizedBusinessException();
		assertEquals("spo.scenario.userUnauthorized", e.getCode());
		assertEquals("User unauthorized to execute this operation.", e.getMessage());
		assertEquals(HttpStatus.NOT_FOUND, e.getHttpStatus());
	}

}
