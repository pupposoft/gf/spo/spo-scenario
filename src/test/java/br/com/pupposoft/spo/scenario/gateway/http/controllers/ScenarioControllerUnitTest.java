package br.com.pupposoft.spo.scenario.gateway.http.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.pupposoft.spo.scenario.domains.Scenario;
import br.com.pupposoft.spo.scenario.domains.User;
import br.com.pupposoft.spo.scenario.gateway.http.controllers.json.ScenarioJson;
import br.com.pupposoft.spo.scenario.usecase.DeleteScenarioUseCase;
import br.com.pupposoft.spo.scenario.usecase.GetScenarioUseCase;
import br.com.pupposoft.spo.scenario.usecase.SaveScenarioUseCase;

@ExtendWith(MockitoExtension.class)
public class ScenarioControllerUnitTest {
	
	@InjectMocks
	private ScenarioController scenarioController;
	
	@Mock
	private SaveScenarioUseCase saveScenarioUseCase;
	
	@Mock
	private GetScenarioUseCase getScenarioUseCase;
	
	@Mock
	private DeleteScenarioUseCase deleteScenarioUseCase;
	
	@Test
	public void createWithSuccess() {
		final String userEmail = "anyUserEmail";
		final String name = "anyName";
		final String description = "anyDescription";
		
		final ScenarioJson scenarioJson = new ScenarioJson(null, name, description, null, null);
		
		final String idToReturn = "anyId"; 
		doReturn(idToReturn).when(saveScenarioUseCase).create(any(Scenario.class), eq(userEmail));
		
		String idReturned = scenarioController.create(scenarioJson, userEmail);
		
		ArgumentCaptor<Scenario> scenarioAC = ArgumentCaptor.forClass(Scenario.class);
		verify(saveScenarioUseCase).create(scenarioAC.capture(), eq(userEmail));
		
		Scenario scenarioCreated = scenarioAC.getValue();
		
		assertNull(scenarioCreated.getId());

		assertEquals(idToReturn, idReturned);
		assertEquals(name, scenarioCreated.getName());
		assertEquals(description, scenarioCreated.getDescription());
		
	}
	
	@Test
	public void getAll() {
		final String userEmail = "anyEmail";
		
		final List<User> users = new ArrayList<>();
		users.add(new User("AA"));
		users.add(new User("BB"));
		users.add(new User("CC"));
		
		final List<Scenario> scenarios = new ArrayList<>();
		scenarios.add(new Scenario("sc1", "casa", null, "u1", users));
		scenarios.add(new Scenario("sc2", "apt", null, "u1", users));
		
		doReturn(scenarios).when(getScenarioUseCase).getAll(userEmail);
		
		List<ScenarioJson> scenariosReturned = scenarioController.getAll(userEmail);
		
		verify(getScenarioUseCase).getAll(userEmail);
		
		assertEquals(2, scenariosReturned.size());
		assertEquals(3, scenariosReturned.get(0).getUsers().size());
		assertEquals(3, scenariosReturned.get(1).getUsers().size());
		
		assertEquals("AA", scenariosReturned.get(0).getUsers().get(0).getId());
		assertEquals("BB", scenariosReturned.get(0).getUsers().get(1).getId());
		assertEquals("CC", scenariosReturned.get(0).getUsers().get(2).getId());
		
		assertEquals("sc1", scenariosReturned.get(0).getId());
		assertEquals("casa", scenariosReturned.get(0).getName());
		assertEquals("u1", scenariosReturned.get(0).getUserIdOwner());
		
		assertEquals("sc2", scenariosReturned.get(1).getId());
		assertEquals("apt", scenariosReturned.get(1).getName());
		assertEquals("u1", scenariosReturned.get(1).getUserIdOwner());
	}
	
	@Test
	public void get() {
		final String userEmail = "anyEmail";
		final String scenarioId = "anyScenarioId";
		final String name = "anyName";
		final String description = "anyDescription";
		final String userIdOwner = "anyIdOwner";
		final List<User> users = new ArrayList<>();
		users.add(new User("anyUserId"));
		
		final Scenario scenario = new Scenario(scenarioId, name, description, userIdOwner, users);
		doReturn(scenario).when(getScenarioUseCase).get(scenarioId, userEmail);
		
		Scenario scenarioFound = scenarioController.get(scenarioId, userEmail);
		
		verify(getScenarioUseCase).get(scenarioId, userEmail);
		
		assertEquals(scenarioId, scenarioFound.getId());
		assertEquals(name, scenarioFound.getName());
		assertEquals(description, scenarioFound.getDescription());
		assertEquals(userIdOwner, scenarioFound.getUserIdOwner());
		assertEquals(users.get(0).getId(), scenarioFound.getUsers().get(0).getId());
		assertEquals(1, scenarioFound.getUsers().size());
	}
		
	@Test
	public void update() {
		final String userEmail = "anyEmail";
		final String scenarioId = "anyId";
		final String name = "anyName";
		final String description = "anyDescription";
		
		final ScenarioJson scenarioJson = new ScenarioJson(scenarioId, name, description, null, null);
		
		scenarioController.update(scenarioJson, userEmail);
		
		ArgumentCaptor<Scenario> scenarioAC = ArgumentCaptor.forClass(Scenario.class);
		verify(saveScenarioUseCase).update(scenarioAC.capture(), eq(userEmail));
		
		Scenario scenarioToUpdate = scenarioAC.getValue();
		
		assertEquals(scenarioId, scenarioToUpdate.getId());
		assertEquals(name, scenarioToUpdate.getName());
		assertEquals(description, scenarioToUpdate.getDescription());
	}
	
	@Test
	public void delete() {
		final String scenarioId = "anyScenarioId";
		final String userEmail = "anyEmail";
		
		scenarioController.delete(scenarioId, userEmail);
		
		verify(deleteScenarioUseCase).delete(scenarioId, userEmail);
	}
}
