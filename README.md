# SPO - Sistema de Previsão de Orçamento

É um sistema que auxilia no controle financeiro pessoal e, garante maior visibilidade de suas contas e gastos a longo prazo.


## Funcionamento
A definir


## Regras
A definir


## Testes

A aplicação foi desenvolvida com TDD, e nela contém diversos testes unitários (automatizados).

Importante ressaltar que no build da aplicação, todos os testes são executados automaticamente.


## Status

Projeto em desenvolvimento.


## Informações Técnicas

O sistema foi desenvolvido com as seguintes metodologias:

- TDD
- Clean Architecture

Tecnologias utilizadas no desenvolvimento do projeto:

- Java 11
- Spring Boot
- Maven
- Lombok (*** ATENÇÃO: É NECESSÁRIO TER O PLUGIN NA IDE, PARA EVITAR ERROS DE COMPILAÇÃO ***)
- JUnit 5
- Mockito
- Git (Gitflow)
- Eclipse (IDE)


## Construção (Build)

Para buildar o projeto é necessário ter o Java e Maven instalados na máquina. Então, execute o comando: 'mvn clean install'
```sh
$ mvn clean install
```

Para execução do projeto, execute o seguinte comando:
```sh
$ java -jar ./
```
